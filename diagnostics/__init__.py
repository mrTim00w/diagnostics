import numpy as np

np.set_printoptions(threshold=5)

from .classes import TimeSerie
from .classes import BooleanTimeSerie
from .classes import StateChangeArray
from .classes import Report
from .classes import Event
